package net.javaguides.springboot.Service;

import java.util.List;

import net.javaguides.springboot.model.Employee;

public interface EmployeeService {
	Employee saveEmployee(Employee employee);
	List<Employee> getAllEmployees();
	Employee getEmployeeByID(long ID);
	Employee updateEmployee(Employee employee,long id);
	void deleteEmployee(long id);

}
