package net.javaguides.springboot.Service.impl;

import java.util.List;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.Service.EmployeeService;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
@Service
public class EmployeeServiceimpl implements EmployeeService{

	private EmployeeRepository employeeRepository;
	
	
	public EmployeeServiceimpl(EmployeeRepository emlpyeeRepository) {
		super();
		this.employeeRepository = emlpyeeRepository;
	}


	@Override
	public Employee saveEmployee(Employee employee) {		
		return employeeRepository.save(employee);
	}


	@Override
	public List<Employee> getAllEmployees() {

		return employeeRepository.findAll();
	}


	@Override
	public Employee getEmployeeByID(long ID) {
	//	Optional<Employee> employee = employeeRepository.findById(ID);
		//if(employee.isPresent())	
		//return employee.get();
		//else throw new ResourceNotFoundException("Employee","id",ID);
	return employeeRepository.findById(ID).orElseThrow(()-> 
	                                                    new ResourceNotFoundException("Employee","id",ID));
	}


	@Override
	public Employee updateEmployee(Employee employee, long id) {
	
		//we need to check whether employee with given id is exist in the DB or not
		Employee existingEmployee = employeeRepository.findById(id).orElseThrow(
				()-> new ResourceNotFoundException("Employee","id",id));
 
		existingEmployee.setFirstName(employee.getFirstName());
		existingEmployee.setLastName(employee.getLastName());
		existingEmployee.setEmail(employee.getEmail());
		
		//save existing employee to DB
		employeeRepository.save(existingEmployee);
		return null;
	}


	@Override
	public void deleteEmployee(long id) {
        employeeRepository.findById(id).orElseThrow(
				()-> new ResourceNotFoundException("Employee","id",id));

		employeeRepository.deleteById(id);
	}

	
}
